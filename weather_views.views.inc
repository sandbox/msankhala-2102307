<?php
/**
 * @file
 * Implemente hook_views_data() to expose weather_location and weather_metar
 * table to view.
 */

/**
 * Implements hook_views_data().
 */
function weather_views_views_data() {
  // Define weather_location table as base table.
  $data['weather_location']['table'] = array(
    'group' => t('Weather'),
    'base' => array(
      // This is the identifier field for the view.
      'field' => 'id',
      'title' => t('Weather'),
      'help' => t('weather_location table contains numeric location id and can be related to weather_metar table.'),
    ),
  );
  // Add field to views from weather_location table.
  // Add id field from weather_location table to views.
  $data['weather_location']['id'] = array(
    'title' => t('Weather location id'),
    'help' => t('Numeric Location ID. Its auto increament primary key of weather_location table.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  // Add icao field from weather_location table to views.
  $data['weather_location']['icao'] = array(
    'title' => t('ICAO code'),
    'help' => t('ICAO code of the METAR station'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  // Add real_name field from weather_location table to views.
  $data['weather_location']['real_name'] = array(
    'title' => t('Location name'),
    'help' => t('Location name set by admin'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort'),
  );

  // Join weather_location table to weather_metar table.
  $data['weather_metar']['table']['group'] = t('Weather');
  $data['weather_metar']['table']['join'] = array(
    'weather_location' => array(
      'left_field' => 'icao',
      'field' => 'icao',
    ),
  );

  // Add field to views from weather_location table.
  // Add temperature field from weather_metar table to views.
  $data['weather_metar']['temperature'] = array(
    'title' => t('Temperature'),
    'help' => t('temperature field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  // Add dewpoint field from weather_metar table to views.
  $data['weather_metar']['dewpoint'] = array(
    'title' => t('Dewpoint'),
    'help' => t('dewpoint field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  // Add sky_condition field from weather_metar table to views.
  $data['weather_metar']['sky_condition'] = array(
    'title' => t('Sky Condition'),
    'help' => t('sky_condition field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  // Add wind_speed field from weather_metar table to views.
  $data['weather_metar']['wind_speed'] = array(
    'title' => t('Wind Speed'),
    'help' => t('wind_speed field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  // Add wind_direction field from weather_metar table to views.
  $data['weather_metar']['wind_direction'] = array(
    'title' => t('Wind Direction'),
    'help' => t('wind_direction field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  // Add image field from weather_metar table to views.
  $data['weather_metar']['image'] = array(
    'title' => t('image name'),
    'help' => t('image field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  // Add reported_on field from weather_metar table to views.
  $data['weather_metar']['reported_on'] = array(
    'title' => t('Reported on'),
    'help' => t('reported_on field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  // Add next_update_on field from weather_metar table to views.
  $data['weather_metar']['next_update_on'] = array(
    'title' => t('Next update on'),
    'help' => t('next_update_on field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  // Add phenomena field from weather_metar table to views.
  $data['weather_metar']['phenomena'] = array(
    'title' => t('Phenomena'),
    'help' => t('phenomena field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  // Add wind_gusts on field from weather_metar table to views.
  $data['weather_metar']['wind_gusts'] = array(
    'title' => t('Wind Gusts'),
    'help' => t('wind_gusts field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  // Add pressure on field from weather_metar table to views.
  $data['weather_metar']['pressure'] = array(
    'title' => t('Pressure'),
    'help' => t('pressure field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  // Add visibility on field from weather_metar table to views.
  $data['weather_metar']['visibility'] = array(
    'title' => t('visibility'),
    'help' => t('visibility field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );

  // Add sunrise_on on field from weather_metar table to views.
  $data['weather_metar']['sunrise_on'] = array(
    'title' => t('sunrise_on'),
    'help' => t('Sunrise on field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );

  // Add sunset_on on field from weather_metar table to views.
  $data['weather_metar']['sunset_on'] = array(
    'title' => t('Sunset on'),
    'help' => t('sunset_on field for a location.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );


  return $data;
}
