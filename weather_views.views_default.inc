<?php

/**
 * @file
 * Default views for Weather listing of added location.
 */

/**
 * Implements hook_views_default_views().
 */
function weather_views_views_default_views() {
  $views = array();

  include dirname(__FILE__) . '/includes/view.weather_views.inc';
  $views[$view->name] = $view;
  return $views;
}
