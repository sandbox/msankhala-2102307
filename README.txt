Weather Views
-------------
This module show the weather data in views. This module has the Weather module  and views_php as dependency. In future it may not have views_php as dependancy.

This module exposes the weather_location and weather_metar table created by weather module to views.
