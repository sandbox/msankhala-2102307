<?php
$view = new view();
$view->name = 'weather';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'weather_location';
$view->human_name = 'weather';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'id' => 'id',
  'dewpoint' => 'dewpoint',
  'icao' => 'icao',
  'image' => 'image',
  'real_name' => 'real_name',
  'sky_condition' => 'sky_condition',
  'temperature' => 'temperature',
  'wind_speed' => 'wind_speed',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'dewpoint' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'icao' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'image' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'real_name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'sky_condition' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'temperature' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'wind_speed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Weather: Weather location id */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'weather_location';
$handler->display->display_options['fields']['id']['field'] = 'id';
/* Field: Weather: image name */
$handler->display->display_options['fields']['image']['id'] = 'image';
$handler->display->display_options['fields']['image']['table'] = 'weather_metar';
$handler->display->display_options['fields']['image']['field'] = 'image';
$handler->display->display_options['fields']['image']['exclude'] = TRUE;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_1']['id'] = 'php_1';
$handler->display->display_options['fields']['php_1']['table'] = 'views';
$handler->display->display_options['fields']['php_1']['field'] = 'php';
$handler->display->display_options['fields']['php_1']['label'] = 'Image';
$handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_1']['php_output'] = '<?php
$weather_image_directory = variable_get(\'weather_image_directory\', drupal_get_path(\'module\', \'weather_views\') . \'images\');
if ( empty( $weather_image_directory )){
$weather_image_directory = \'\'. drupal_get_path(\'module\', \'weather\') . \'/images/\';
}
print \'<img src="/\' . $weather_image_directory . $row->image . \'" alt="\' . $row->image . \'.png"/>\';
?>';
$handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
/* Field: Weather: Location name */
$handler->display->display_options['fields']['real_name']['id'] = 'real_name';
$handler->display->display_options['fields']['real_name']['table'] = 'weather_location';
$handler->display->display_options['fields']['real_name']['field'] = 'real_name';
/* Field: Weather: ICAO code */
$handler->display->display_options['fields']['icao']['id'] = 'icao';
$handler->display->display_options['fields']['icao']['table'] = 'weather_location';
$handler->display->display_options['fields']['icao']['field'] = 'icao';
/* Field: Weather: Dewpoint */
$handler->display->display_options['fields']['dewpoint']['id'] = 'dewpoint';
$handler->display->display_options['fields']['dewpoint']['table'] = 'weather_metar';
$handler->display->display_options['fields']['dewpoint']['field'] = 'dewpoint';
/* Field: Weather: Temperature */
$handler->display->display_options['fields']['temperature']['id'] = 'temperature';
$handler->display->display_options['fields']['temperature']['table'] = 'weather_metar';
$handler->display->display_options['fields']['temperature']['field'] = 'temperature';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = 'Humidity';
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php
print weather_views_format_relative_humidity($row->temperature, $row->dewpoint);
?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Field: Weather: Sky Condition */
$handler->display->display_options['fields']['sky_condition']['id'] = 'sky_condition';
$handler->display->display_options['fields']['sky_condition']['table'] = 'weather_metar';
$handler->display->display_options['fields']['sky_condition']['field'] = 'sky_condition';
/* Field: Weather: Pressure */
$handler->display->display_options['fields']['pressure']['id'] = 'pressure';
$handler->display->display_options['fields']['pressure']['table'] = 'weather_metar';
$handler->display->display_options['fields']['pressure']['field'] = 'pressure';
$handler->display->display_options['fields']['pressure']['date_format'] = 'short';
/* Field: Weather: Reported on */
$handler->display->display_options['fields']['reported_on']['id'] = 'reported_on';
$handler->display->display_options['fields']['reported_on']['table'] = 'weather_metar';
$handler->display->display_options['fields']['reported_on']['field'] = 'reported_on';
$handler->display->display_options['fields']['reported_on']['date_format'] = 'short';
/* Field: Weather: Next update on */
$handler->display->display_options['fields']['next_update_on']['id'] = 'next_update_on';
$handler->display->display_options['fields']['next_update_on']['table'] = 'weather_metar';
$handler->display->display_options['fields']['next_update_on']['field'] = 'next_update_on';
$handler->display->display_options['fields']['next_update_on']['date_format'] = 'short';
/* Field: Weather: sunrise_on */
$handler->display->display_options['fields']['sunrise_on']['id'] = 'sunrise_on';
$handler->display->display_options['fields']['sunrise_on']['table'] = 'weather_metar';
$handler->display->display_options['fields']['sunrise_on']['field'] = 'sunrise_on';
$handler->display->display_options['fields']['sunrise_on']['date_format'] = 'short';
/* Field: Weather: Sunset on */
$handler->display->display_options['fields']['sunset_on']['id'] = 'sunset_on';
$handler->display->display_options['fields']['sunset_on']['table'] = 'weather_metar';
$handler->display->display_options['fields']['sunset_on']['field'] = 'sunset_on';
$handler->display->display_options['fields']['sunset_on']['date_format'] = 'short';
/* Field: Weather: visibility */
$handler->display->display_options['fields']['visibility']['id'] = 'visibility';
$handler->display->display_options['fields']['visibility']['table'] = 'weather_metar';
$handler->display->display_options['fields']['visibility']['field'] = 'visibility';
/* Field: Weather: Wind Speed */
$handler->display->display_options['fields']['wind_speed']['id'] = 'wind_speed';
$handler->display->display_options['fields']['wind_speed']['table'] = 'weather_metar';
$handler->display->display_options['fields']['wind_speed']['field'] = 'wind_speed';
/* Field: Weather: Wind Direction */
$handler->display->display_options['fields']['wind_direction']['id'] = 'wind_direction';
$handler->display->display_options['fields']['wind_direction']['table'] = 'weather_metar';
$handler->display->display_options['fields']['wind_direction']['field'] = 'wind_direction';
/* Field: Weather: Wind Gusts */
$handler->display->display_options['fields']['wind_gusts']['id'] = 'wind_gusts';
$handler->display->display_options['fields']['wind_gusts']['table'] = 'weather_metar';
$handler->display->display_options['fields']['wind_gusts']['field'] = 'wind_gusts';
/* Field: Weather: Phenomena */
$handler->display->display_options['fields']['phenomena']['id'] = 'phenomena';
$handler->display->display_options['fields']['phenomena']['table'] = 'weather_metar';
$handler->display->display_options['fields']['phenomena']['field'] = 'phenomena';
